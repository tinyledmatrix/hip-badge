# Badge development

To develop code on the badge, you need to set up a development environment (using ESP IDF for the compiling and linking), and you need to make sure that when code has been compiled you can flash the badge.

The following instructions are based on the assumption you are using a Linux system. WSL (Windows Subsystem for Linux) will not work, since it is unable to use the USB device.

## Connecting your device
In a terminal, do
```
sudo tail -f /var/log/syslog | grep tty
```

depending on your distro you might prefer to use

```
sudo dmesg -w | grep tty
```

Switch your badge on, then connect it to your machine.

The terminal should show some lines giving you a hint at which unix device you will be able to reach your badge. Typically, this would be something like `/dev/ttyACM0`.

Whereever the instructions below say `PORT`, insert `/dev/ttyACM0` or whatever you took from the syslog.

## Using ESP IDF natively

Development is done on the v4.4.x branch of the ESP-IDF. If you have not
installed the ESP-IDF, please follow the excellent instructions at
https://docs.espressif.com/projects/esp-idf/en/v4.4.3/esp32c3/get-started/index.html or see below for a docker based installation.

If you already have an installed ESP-IDF, check out the latest v4.4.x tag:

```
$ cd $IDF_PATH
$ git fetch
$ git checkout v4.4.3
```

(at this point you might have some residual files lying around in components/
 and examples/. You should very carefully inspect each and everyo NOOOO!!!!!!
 Nuke them from orbit: rm -rf components/ examples/; git checkout .)

```
$ git submodule update --init --recursive
$ bash install.sh
```

## Use ESP IDF in a docker image

You can also use a docker version of it. Create this little shell script, which calls `idf.py` inside docker instead of directly (remember to replace `PORT` by the port that your badge appears at):

(idf.py)
```
#!/bin/bash
# as per https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/tools/idf-docker-image.html
sudo docker run -v $PWD:/project -w /project --device PORT espressif/idf:release-v4.4 idf.py $*
```

Call this script instead of `idf.py` later on in the documentation.

## Checking out and modifying the firmware

To get started with your HiP Badge on a computer with a ESP-IDF installation:

```
$ git clone https://git.sr.ht/~rcs/HiP-Badge
$ cd HiP-Badge/firmware
$ git submodule update --init --recursive
$ . /path/to/your/esp-idf/export.sh # this does not seem to be necessary nor effective
$ cd blinkenlights
$ idf.py set-target esp32c3
$ idf.py menuconfig --help (appearance with --style)
$ idf.py menuconfig (generates a sdkconfig file)
```

You should now be setup for development. The main file with blinkenlight is located in `main/hipbadge.c`. Make the changes you need, then...

```
$ idf.py build
```
and if that threw no errors, connect your device and do
```
$ idf.py -p PORT flash # usually, PORT would be /dev/ttyACM0
```
If this did not work, you can try to do
```
$ idf.py -p PORT erase-flash
$ idf.py -p PORT erase-otadata
$ idf.py -p PORT flash
```

Right after flashing, your device should reboot and operate with your changes.
